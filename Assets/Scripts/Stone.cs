﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stone : MonoBehaviour{

	public int hardness;
	//use 0 if unbreakable
	public int richness;
	[Tooltip("Its size must be its hardness-1")]
	public Sprite[] rockPictures;
	// its length should equal to its hardness - 1
	public GameObject sparkPrefeb;
	public AudioClip stoneSound;
	public AudioClip stoneFall;
	public GameObject gold;
	public GameObject silver;
	private int hits;
	private GameObject sparkEffect;
	private GameObject coins;
	private int chance;
	private StoneNumber stoneNumber;

	void Awake(){
		stoneNumber = GameObject.FindObjectOfType<StoneNumber>();
		if(hardness != 0){
			stoneNumber.IncreaseStone();
			//StoneNumber.breakableStoneNumber++;
		}
	}

	// Use this for initialization
	void Start(){
		sparkEffect = GameObject.FindGameObjectWithTag("Sparks");
		coins = GameObject.FindGameObjectWithTag("Coins");

	}

	void HandleHits(){
		if(hits >= hardness){
			GameObject newSpark = Instantiate(sparkPrefeb, sparkEffect.transform);
			newSpark.transform.position = this.transform.position;
			AudioSource.PlayClipAtPoint(stoneFall, this.gameObject.transform.position, 1f);
			HandleCoinCreation();
			stoneNumber.DecreaseStone();
			//StoneNumber.breakableStoneNumber--;
			stoneNumber.UpdateStone();
			Destroy(this.gameObject);
		} else{
			LoadSprite();
		}
	}

	void OnCollisionEnter2D(){
		hits++;
		AudioSource.PlayClipAtPoint(stoneSound, this.gameObject.transform.position, 0.5f);
		if(hardness != 0){
			HandleHits();
		}
	}

	private void LoadSprite(){
		gameObject.GetComponent<SpriteRenderer>().sprite = rockPictures[hits - 1];
	}

	private void CreatGoldCoin(){
		GameObject goldCoin = Instantiate(gold, coins.transform);
		goldCoin.transform.position = gameObject.transform.position;
	}

	private void CreatSilverCoin(){
		GameObject silverCoin = Instantiate(silver, coins.transform);
		silverCoin.transform.position = gameObject.transform.position;
	}

	private void HandleCoinCreation(){
		chance = Random.Range(0, richness);
		if(chance >= 8){
			CreatGoldCoin();
			CreatGoldCoin();
			CreatSilverCoin();
			CreatSilverCoin();
			CreatSilverCoin();
		} else if(chance >= 6){
			CreatGoldCoin();
			CreatSilverCoin();
		} else if(chance >= 4){
			CreatSilverCoin();
			CreatSilverCoin();
		} else if(chance >= 2){
			CreatSilverCoin();
		}

	}
}
