﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour{

	public AudioClip ballSound;
	private GameObject paddle;
	private Vector3 paddleToBall;
	private bool gameStarted = false;
	// Use this for initialization
	void Start(){
		paddle = GameObject.FindGameObjectWithTag("Paddle");
		//paddleToBall = this.transform.position - paddle.transform.position;
		paddleToBall = new Vector3(0f,0.61f,0f);
	}
	
	// Update is called once per frame
	void Update(){
		if(!gameStarted){
			this.transform.position = paddle.transform.position + paddleToBall;
			if(Input.GetMouseButtonUp(0)){
				this.GetComponent<Rigidbody2D>().velocity = new Vector2(Random.Range(-2f, 2f), 12f);
				gameStarted = true;
			}
		}
	}
	
	//Ball audio plays when ball hits anything
	void OnCollisionEnter2D(Collision2D ballHit){
		Vector2 tweak = new Vector2(Random.Range(-0.3f, 0.3f), Random.Range(0f, 0.2f));
		if(gameStarted){
			//GetComponent<AudioSource>().Play();
			AudioSource.PlayClipAtPoint(ballSound,gameObject.transform.position);
			this.GetComponent<Rigidbody2D>().velocity += tweak;
		}
	}
}
