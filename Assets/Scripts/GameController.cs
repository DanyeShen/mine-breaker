﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour{

	private MusicManager musicManager;

	void Awake(){
		musicManager = GameObject.FindObjectOfType<MusicManager>();
	}

	// Use this for initialization
	void Start(){
		musicManager.PlaySceneMusic();
	}

}
