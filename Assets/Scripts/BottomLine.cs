﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BottomLine : MonoBehaviour {

	public GameObject ball;
	public static int life = 3 ;
	private GameObject lifeText;
	private LevelManager levelManager;
	//private GoldNumber goldNumber;

	void Start(){
		levelManager = GameObject.FindObjectOfType<LevelManager>();
		lifeText = GameObject.Find("LifeText");
		UpdateLife();
		//goldNumber = GameObject.FindObjectOfType<GoldNumber>();
	}

	void OnTriggerEnter2D(Collider2D collider){
		if(collider.tag=="Ball"){
			if(life<=0){
				life = 3;
				levelManager.LoadLose();
			}else{
				life--;
				UpdateLife();
				Instantiate(ball);
			}
		}
		Destroy(collider.gameObject);
	}

	private void UpdateLife(){
		lifeText.GetComponent<Text>().text = life.ToString();
	}
}
