﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainPlayPanel : MonoBehaviour{

	private GameObject userButton;
	private GameObject paddle;
	private Camera gameCamera;

	private Vector3 newButtonValue;
	private Vector3 newPaddleValue;
	private float fixedButtonYValue;
	private float fixedPaddleYValue;

	// Use this for initialization
	void Start(){
		gameCamera = GameObject.FindObjectOfType<Camera>();
		userButton = GameObject.FindGameObjectWithTag("UserButton");
		paddle = GameObject.FindGameObjectWithTag("Paddle");
		fixedButtonYValue = userButton.transform.position.y;
		fixedPaddleYValue = paddle.transform.position.y;
	}
	
	// Update is called once per frame
	void Update(){
		
	}

	void OnMouseDown(){
		Time.timeScale = 1f;
	}

	void OnMouseUp(){
		Time.timeScale = 0.2f;
	}

	void OnMouseDrag(){
		Vector3 mousePosition = gameCamera.ScreenToWorldPoint(Input.mousePosition);
		newButtonValue = new Vector3(Mathf.Clamp(mousePosition.x, 1f, 8f), fixedButtonYValue, -1f);
		newPaddleValue = new Vector3(Mathf.Clamp(mousePosition.x, 1.25f, 7.75f), fixedPaddleYValue, -1f);
		userButton.transform.position = newButtonValue;
		paddle.transform.position = newPaddleValue;
	}
}
