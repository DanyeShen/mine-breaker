﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeSlider : MonoBehaviour{

	private AudioSource musicPlayer;
	private Slider slider;

	// Use this for initialization
	void Start(){
		musicPlayer = GameObject.FindObjectOfType<MusicManager>().GetComponent<AudioSource>();
		slider = this.GetComponent<Slider>();
		slider.value = musicPlayer.volume;
	}
	
	// Update is called once per frame
	void Update(){
		musicPlayer.volume = slider.value;
	}

	public void SetDefaultVolume(){
		slider.value = 1f;
	}
}
