﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoneNumber : MonoBehaviour {

	public int breakableStoneNumber;
	private Text text;
	private LevelManager levelManager;
	private GameObject winMessage;
	private GameObject bottomLine;

	void Start(){
		text = GetComponent<Text>();
		levelManager = GameObject.FindObjectOfType<LevelManager>();
		winMessage = GameObject.Find("WinMessage");
		winMessage.SetActive(false);
		bottomLine = GameObject.Find("BottomLine");
		bottomLine.SetActive(true);
		UpdateStone();
	}

	public void UpdateStone(){
		text.text = breakableStoneNumber.ToString();
		if(breakableStoneNumber<=0){
			bottomLine.SetActive(false);
			winMessage.SetActive(true);
			breakableStoneNumber=0;
			Invoke("LevelClear",3f);
		}
	}

	public void IncreaseStone(){
		breakableStoneNumber++;
	}

	public void DecreaseStone(){
		breakableStoneNumber--;
	}

	private void LevelClear(){
		levelManager.LoadNextLevel();
	}

}