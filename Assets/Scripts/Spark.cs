﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spark : MonoBehaviour{

	public Sprite[] sparks;
	private int timesBurn;
	private SpriteRenderer currentRenderer;
	private float frameRate = 0.1f;

	// Use this for initialization
	void Start(){
		timesBurn = 0;
		currentRenderer = gameObject.GetComponent<SpriteRenderer>();
		Invoke("LoadNextSpark", frameRate);
	}

	private void LoadNextSpark(){
		currentRenderer.sprite = sparks[timesBurn];
		timesBurn++;
		if(timesBurn < sparks.Length){
			Invoke("LoadNextSpark", frameRate);
		} else{
			Invoke("BurnOut", frameRate);
		}
	}

	private void BurnOut(){
		Destroy(this.gameObject);
	}
}
