﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {

	public int value;
	public AudioClip coinSound;
	private GoldNumber goldNumber;

	void Start(){
		this.GetComponent<Rigidbody2D>().velocity += new Vector2(Random.Range(-1f, 1f), Random.Range(-2f, 2f));
		goldNumber = GameObject.FindObjectOfType<GoldNumber>();
	}

	void OnTriggerEnter2D(Collider2D collider){
		if(collider.tag == "Paddle"){
			AudioSource.PlayClipAtPoint(coinSound,this.transform.position,1f);
			goldNumber.gotCoin(value);
			Destroy(this.gameObject);
		}
	}
}
