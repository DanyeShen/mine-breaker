﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoldNumber : MonoBehaviour {

	public static int coins;
	private Text text;

	void Start(){
		text = GetComponent<Text>();
		UpdateGold();
	}

	public void UpdateGold(){
		text.text = coins.ToString();
	}

	public void gotCoin(int gold){
		coins += gold;
		UpdateGold();
	}

	public void restartCoin(){
		coins = 0;
		UpdateGold();
	}

	public static int getCoins(){
		return coins;
	}
}
